package com.exaltit.kata;

import com.exaltit.kata.domain.OperationResponse;
import com.exaltit.kata.domain.enumeration.OperationType;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class OperationResponseTest {

    @Test
    public void test_return_description() {
        LocalDateTime dateTime = LocalDateTime.of(2022, 12, 10, 12, 23, 34);
        OperationResponse response = new OperationResponse(null, new BigDecimal("20"), OperationType.DEPOSIT, dateTime);
        Assertions.assertEquals("Dépôt de 20€ le 10/12/2022 à 12:23:34", response.getDescription());
    }
}
