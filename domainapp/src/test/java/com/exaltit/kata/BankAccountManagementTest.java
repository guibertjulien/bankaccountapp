package com.exaltit.kata;

import com.exaltit.kata.domain.BankAccount;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BankAccountManagementTest {

    private BankAccount bankAccount;

    @BeforeAll
    public void setup() {
        bankAccount = new BankAccount(new BigDecimal("100"));
    }

    @ParameterizedTest
    @ValueSource(longs = {20, 40, 60, 80, 100})
    void test_check_account_balance_should_return_false_when_amount_is_lass_than_or_equals_current_credit(long iAmount) {
        assertFalse(bankAccount.isCreditLessThanAmount(new BigDecimal(iAmount), bankAccount.getCredit()));
    }

    @ParameterizedTest
    @ValueSource(longs = {120, 140})
    void test_check_account_balance_should_return_true_when_amount_is_greater_than_current_credit(long iAmount) {
        assertTrue(bankAccount.isCreditLessThanAmount(new BigDecimal(iAmount), bankAccount.getCredit()));
    }

}
