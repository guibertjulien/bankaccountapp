package com.exaltit.kata.business;

import com.exaltit.kata.domain.*;

import java.util.UUID;

public interface BankAccountBusiness {

    /**
     * create a bank account
     * @param request created bank account request
     * @return created BankAccount
     */
    BankAccount create(CreatedBankAccountRequest request);

    /**
     * make operation in specific account
     * @param bankAccountId id of specific bank account on which the operation will be done
     * @param request the object operation request
     * @return Operation response payload
     */
    OperationResponse makeOperation(UUID bankAccountId, OperationRequest request);
}
