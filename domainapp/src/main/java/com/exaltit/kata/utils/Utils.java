package com.exaltit.kata.utils;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UUID;

public class Utils {

    public  static final DateTimeFormatter FRENCH_DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy à hh:mm:ss");

    public static final NumberFormat DECIMAL_FORMATTER = NumberFormat.getInstance(Locale.FRANCE);

    public static final UUID BANK_ACCOUNT_EXAMPLE_ID = UUID.fromString("a0601c34-23e7-43b4-9646-fe2f00467323");
}
