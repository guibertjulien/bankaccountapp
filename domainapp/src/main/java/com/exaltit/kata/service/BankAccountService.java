package com.exaltit.kata.service;

import com.exaltit.kata.domain.BankAccount;

import java.util.List;
import java.util.UUID;

public interface BankAccountService {

    /**
     * find all bank accounts
     * @return list of bank accounts created
     */
    List<BankAccount> findAll();

    /**
     * find bak account by id
     * @param id specific bank account id
     * @return object of bank account
     */
    BankAccount findById(UUID id);

    /**
     * save bank account
     * @param account the object of bank account to be save
     * @return the saved bank account
     */
    BankAccount save(BankAccount account);
}
