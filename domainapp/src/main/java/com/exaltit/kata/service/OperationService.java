package com.exaltit.kata.service;

import com.exaltit.kata.domain.Operation;

import java.util.List;
import java.util.UUID;

public interface OperationService {

    /**
     * find all operations by accountId
     * @param bankAccountId specific id of bank account
     * @return list of operations
     */
    List<Operation> finAllByAccountId(UUID bankAccountId);
}
