package com.exaltit.kata.domain;

import com.exaltit.kata.domain.enumeration.OperationType;
import com.exaltit.kata.exception.InsufficientCreditException;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class BankAccount {

    private UUID id;

    private BigDecimal credit;

    private List<Operation> operations = new ArrayList<>();

    public BankAccount(BigDecimal credit) {
        this.credit = credit;
    }

    public void addOperations(final Operation newOperation) {
        this.operations.add(newOperation);
    }

    public BankAccount doOperation(Operation operation) {
        BigDecimal newCredit;
        if(OperationType.WITHDRAWAL.equals(operation.getType())) {
            if(isCreditLessThanAmount(operation.getAmount(), this.credit)) {
                throw new InsufficientCreditException("Insufficient funds. The withdrawal amount must be less than or equal to your current amount");
            }
            newCredit= this.credit.subtract(operation.getAmount());

        } else {
            newCredit = this.credit.add(operation.getAmount());
        }

        this.credit = newCredit;
        operation.setBalance(newCredit);
        this.addOperations(operation);

        return this;
    }

    public static boolean isCreditLessThanAmount(BigDecimal amount, BigDecimal credit) {
        return credit.compareTo(amount) < 0;
    }
}
