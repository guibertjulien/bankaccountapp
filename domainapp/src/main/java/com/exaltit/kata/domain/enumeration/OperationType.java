package com.exaltit.kata.domain.enumeration;

public enum OperationType {

    DEPOSIT("Dépôt"),
    WITHDRAWAL("Retrait");

    private String label;

    OperationType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
