package com.exaltit.kata.domain;

import com.exaltit.kata.domain.enumeration.OperationType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter @Setter @NoArgsConstructor
public class Operation {

    @JsonIgnore
    private UUID id;

    private OperationType type;

    private BigDecimal amount;

    @JsonIgnore
    private BigDecimal balance;

    @JsonIgnore
    private LocalDateTime createdAt;
}
