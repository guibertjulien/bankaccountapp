package com.exaltit.kata.domain;

import com.exaltit.kata.domain.enumeration.OperationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter @NoArgsConstructor @AllArgsConstructor
public class OperationRequest {

    private OperationType type;

    private BigDecimal amount;
}
