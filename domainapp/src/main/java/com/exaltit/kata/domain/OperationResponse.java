package com.exaltit.kata.domain;

import com.exaltit.kata.domain.enumeration.OperationType;
import com.exaltit.kata.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter @AllArgsConstructor
public class OperationResponse {

    private BigDecimal balance;

    @JsonIgnore
    private BigDecimal amount;

    private OperationType type;

    @JsonIgnore
    private LocalDateTime createdAt;

    public String getDescription() {
        return this.type.getLabel() + " de " + Utils.DECIMAL_FORMATTER.format(this.amount) + "€ le " +
                (this. createdAt != null ? Utils.FRENCH_DATE_FORMATTER.format(this.createdAt) : Utils.FRENCH_DATE_FORMATTER.format(LocalDateTime.now()));
    }
}
