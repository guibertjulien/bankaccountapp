package com.exaltit.kata.exception;

public class NullValueException extends RuntimeException{

    public NullValueException(String message) {
        super(message);
    }
}
