# Bank Account Kata

## Récupération du projet

``` bash
git clone https://gitlab.com/HichamMEDDOUR/bankaccountapp.git
```
## Build le projet avec mvn 

en positionnant sur la racine du projet lancer la commande suivant :

``` bash
mvn clean install
```

Lancer la classe principale BankAccountApiApplication.class

## Postman (Tester les routes)

**POST**

Dans un premier temps, création d'un compte bancaire

http://localhost:8080/v1/bank_accounts 
 
Body :
 
 ```
 {
    "credit": 100
 }
 ```

**GET**

Retourner tous les comptes bancaires créés

http://localhost:8080/v1/bank_accounts 

**GET**

Retourner les informations d'un compte bancaire en passant un id qui n'existe pas en base de données

http://localhost:8080/v1/bank_accounts/:id

l'api va retourner une erreur 404, car aucun compte n'est lié à cet id

Voici un exemple d'erreur qui sera retourné 

 ```
 {
     "timestamp": "2022-10-24T08:36:32.690+00:00",
     "status": 404,
     "error": "Not Found",
     "message": "No resource found for bank account with id 5cc08449-7ab0-41ba-a4ca-00ce1cdde351.",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351"
 }
 ```

**GET**

Retourner les informations d'un compte bancaire en listant toutes les opérations effectuées on ordre décoissant

http://localhost:8080/v1/bank_accounts/:id

 ```
 {
     "id": "5cc08449-7ab0-41ba-a4ca-ce1cdde351ad",
     "credit": 80.00,
     "operations": [
         {
             "id": "cfe92913-7413-4cd5-a611-1e49e7ab594d",
             "type": "WITHDRAWAL",
             "amount": 20.00,
             "balance": 80.00,
             "createdAt": "2022-10-24T10:15:44.966005",
             "description": "Retrait de 20.00€ le 2022-10-24T10:15:44"
         },
         {
             "id": "16538c5c-6dba-4398-85a4-54ba38686169",
             "type": "WITHDRAWAL",
             "amount": 50.00,
             "balance": 100.00,
             "createdAt": "2022-10-24T10:15:41.070527",
             "description": "Retrait de 50.00€ le 2022-10-24T10:15:41"
         },
         {
             "id": "c4c76399-670f-4766-9ac4-c8e0184a5ed0",
             "type": "DEPOSIT",
             "amount": 30.00,
             "balance": 150.00,
             "createdAt": "2022-10-24T10:15:29.399784",
             "description": "Dépôt de 30.00€ le 2022-10-24T10:15:29"
         },
         {
             "id": "d9b60089-27dc-4fda-a367-5ada50e579f6",
             "type": "DEPOSIT",
             "amount": 20.00,
             "balance": 120.00,
             "createdAt": "2022-10-24T10:15:09.124751",
             "description": "Dépôt de 20.00€ le 2022-10-24T10:15:09"
         }
     ]
 }
 ```

Voici un exemple de retour de l'api 

**POST**

Tenter une oépration le compte sans préciser le type de l'opération

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 20
 }
 ```

L'api va retourner une erreur car le type de l'opération est obligatoire

Voici un exemple d'erreur 

 ```
 {
     "timestamp": "2022-10-24T08:27:13.063+00:00",
     "status": 400,
     "error": "Bad Request",
     "message": "Operation type is required",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Dépôt d'un montant négatif sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": -20,
    "type": "DEPOSIT"
 }
 ```

L'api va retourner une erreur car l'opération de déposer un montant négatif n'est pas possible

Voici un exemple de retour 

 ```
 {
     "timestamp": "2022-10-24T08:25:49.694+00:00",
     "status": 400,
     "error": "Bad Request",
     "message": "Operation amount must be greater than 0",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Dépôt d'un montant positif sur le compte créé

http://localhost:8080/v1/bank_accounts/:id/operation

id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 20,
    "type": "DEPOSIT"
 }
 ```

L'api devrra retourner l'objer opération créé

**POST**

Retrait d'un montant négatif sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": -20,
    "type": "WITHDRAWAL"
 }
 ```

L'api va retourner une erreur car l'opération de retrait un montant négatif n'est pas possible

Voici un exemple de retour 

 ```
 {
     "timestamp": "2022-10-24T08:25:49.694+00:00",
     "status": 400,
     "error": "Bad Request",
     "message": "Operation amount must be greater than 0",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Retrait d'un montant positif sur le compte créé et le montant demandé n'est pas disponible sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 120,
    "type": "WITHDRAWAL"
}
 ```

L'api devrra retourner une erreur car nous pouvons pas retirer si on pas assez de fonds suffisants sur le compte

Voici un exemple de retour 

 ```
 {
     "timestamp": "2022-10-24T08:34:32.580+00:00",
     "status": 409,
     "error": "Conflict",
     "message": "Insufficient funds. The withdrawal amount must be less than or equal to your current amount",
     "path": "/v1/bank_accounts/5cc08449-7ab0-41ba-a4ca-ce1cdde351ad/operation"
 }
 ```

**POST**

Retrait d'un montant positif sur le compte créé et le montant demandé est bien disponible sur le compte

http://localhost:8080/v1/bank_accounts/:id/operation

L'id : c'est l'id du compte créé 

Body :

 ```
 {
    "amount": 30,
    "type": "WITHDRAWAL"
}
 ```

L'api devrra retourner l'objer opération créé

**GET**

Voir toutes les opérations effectués d'un comptes bancaire

L'id : c'est l'id du compte créé 

http://localhost:8080/v1/bank_accounts/:id/operations

L'api devrra retourner toutes les opérations effectuées sur ce compte en ordre de création décroissant
