package com.exaltit.kata;

import com.exaltit.kata.business.BankAccountBusiness;
import com.exaltit.kata.business.BankAccountBusinessImpl;
import com.exaltit.kata.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BankAccountApiApplication {

	@Autowired
	private BankAccountService bankAccountService;

	public static void main(String[] args) {
		SpringApplication.run(BankAccountApiApplication.class, args);
	}

	@Bean
	BankAccountBusiness bankAccountBusiness() {
		return new BankAccountBusinessImpl(this.bankAccountService);
	}
}
