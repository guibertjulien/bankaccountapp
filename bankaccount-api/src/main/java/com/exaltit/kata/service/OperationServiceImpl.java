package com.exaltit.kata.service;

import com.exaltit.kata.domain.Operation;
import com.exaltit.kata.entities.OperationEntity;
import com.exaltit.kata.mapper.OperationMapper;
import com.exaltit.kata.repository.OperationEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class OperationServiceImpl implements OperationService {

    @Autowired
    private OperationEntityRepository repository;

    @Autowired
    private OperationMapper operationMapper;

    @Override
    public List<Operation> finAllByAccountId(UUID bankAccountId) {
        List<OperationEntity> entities = this.repository.findAllOperationByBankAccountId(bankAccountId);

        return this.operationMapper.toDomainList(entities);
    }
}
