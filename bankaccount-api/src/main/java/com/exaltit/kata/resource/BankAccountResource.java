package com.exaltit.kata.resource;

import com.exaltit.kata.business.BankAccountBusiness;
import com.exaltit.kata.domain.*;
import com.exaltit.kata.exception.InsufficientCreditException;
import com.exaltit.kata.exception.NegativeValueException;
import com.exaltit.kata.exception.NotFoundException;
import com.exaltit.kata.exception.NullValueException;
import com.exaltit.kata.service.BankAccountService;
import com.exaltit.kata.service.OperationService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/bank_accounts")
@CrossOrigin
public class BankAccountResource {

    @Autowired
    private BankAccountBusiness bankAccountBusiness;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private OperationService operationService;

    @GetMapping
    @ApiResponse(code = 200, message = "The bank account created.", response = BankAccount.class, responseContainer = "List")
    public ResponseEntity<List<BankAccount>> getAll() {
        List<BankAccount> allAccounts = this.bankAccountService.findAll();
        return new ResponseEntity<>(allAccounts, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The bank account returned.", response = BankAccount.class),
            @ApiResponse(code = 404, message = "Bank account not found.", response = NotFoundException.class)})
    public ResponseEntity<BankAccount> getById(@PathVariable("id") UUID accountId) {
        try {
            BankAccount bankAccount = this.bankAccountService.findById(accountId);
            return new ResponseEntity<>(bankAccount, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    @ApiResponse(code = 200, message = "The bank account created.", response = BankAccount.class)
    public ResponseEntity<BankAccount> post(@RequestBody final CreatedBankAccountRequest request) {
        try {
            BankAccount created = bankAccountBusiness.create(request);
            return new ResponseEntity<>(created, HttpStatus.OK);
        } catch (NullValueException | NegativeValueException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{id}/operation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The operation has succeeded.", response = Operation.class),
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 404, message = "Resource not found.", response = NotFoundException.class),
            @ApiResponse(code = 409, message = "Conflict.", response = InsufficientCreditException.class)})
    public ResponseEntity<OperationResponse> doOperation(@PathVariable("id") UUID bankAccountId, @RequestBody OperationRequest request) {
        try {
            OperationResponse response = this.bankAccountBusiness.makeOperation(bankAccountId, request);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (NullValueException | NegativeValueException ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (InsufficientCreditException ex) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping("/{id}/operations")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The bank account returned.", response = Operation.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Bank account not found.", response = NotFoundException.class)})
    public ResponseEntity<List<Operation>> getAllOperations(@PathVariable("id") UUID bankAccountId) {
        try {
            BankAccount account = this.bankAccountService.findById(bankAccountId);
            List<Operation> operations = this.operationService.finAllByAccountId(account.getId());
            return new ResponseEntity<>(operations, HttpStatus.OK);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
