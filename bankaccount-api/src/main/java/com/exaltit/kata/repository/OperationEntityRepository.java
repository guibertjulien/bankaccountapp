package com.exaltit.kata.repository;

import com.exaltit.kata.entities.OperationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OperationEntityRepository extends JpaRepository<OperationEntity, UUID> {

    @Query("select o from OperationEntity o left join fetch o.bankAccount b where b.id = :bankAccountId order by o.createdAt desc")
    List<OperationEntity> findAllOperationByBankAccountId(@Param("bankAccountId") UUID bankAccountId);
}
