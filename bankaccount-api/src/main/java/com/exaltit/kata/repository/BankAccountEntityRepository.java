package com.exaltit.kata.repository;

import com.exaltit.kata.entities.BankAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface BankAccountEntityRepository extends JpaRepository<BankAccountEntity, UUID> {
}
