package com.exaltit.kata.mapper;

import com.exaltit.kata.domain.BankAccount;
import com.exaltit.kata.entities.BankAccountEntity;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.NullValueCheckStrategy;

@org.mapstruct.Mapper(componentModel = "spring", uses = OperationMapper.class,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public abstract class BankAccountMapper implements Mapper<BankAccountEntity, BankAccount> {
}
