package com.exaltit.kata.mapper;

import com.exaltit.kata.domain.Operation;
import com.exaltit.kata.entities.OperationEntity;
import org.mapstruct.NullValueCheckStrategy;

@org.mapstruct.Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public abstract class OperationMapper implements Mapper<OperationEntity, Operation>{
}
